﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCommand
{
    public static  class Constant
    {

      public const string CommandLs= "ls";
      public const string CommandTouch = "touch";
      public const string CommandHelp = "help";
      public const string CommandCd = "cd";
      public const string CommandMv= "mv";
      public const string ModifierR = "-R";
  
    }
}
