﻿using AppCommand.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AppCommand.Interface
{
    interface Icommand
    {
        void Touch(string nameFile);
        void Rename(string nameFile1, string nameFile2);
        IList<String> ListFilesFolders();
        IList<String> ListRecursive(DirectoryInfo directoryInfo, int level);
        void ChangeDirectory(string path);
        IList<Help> Help(string command);
    }
}
