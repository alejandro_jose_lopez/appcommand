﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCommand.entities
{
    public class Help
    {
        public String Key { get; set; }
        public String Text { get; set; }
    }
}
