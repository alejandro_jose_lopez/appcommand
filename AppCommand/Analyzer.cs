﻿using AppCommand.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AppCommand
{
    public static class Analyzer
    {
        public static void Analizer(string[] args)
        {
            Command _command = new Command();

            if (args.Length > 0)
            {
                if (args[0] == Constant.CommandTouch)
                {
                    if (args.Length == 2)
                    {
                        _command.Touch(args[1]);
                        Console.WriteLine("Se creo el archivo " + args[1]);
                    }
                    else
                        Console.WriteLine("Faltan parametros, formato: touch [nombre archivo]");
                }

                if (args[0] ==Constant.CommandLs)
                {
                    String result = string.Empty;
                    IList<String> list = new List<String>();
                    if (args.Length == 2)
                    {
                        if (args[1] == Constant.ModifierR)
                        {
                            DirectoryInfo directoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
                            list = _command.ListRecursive(directoryInfo, 0);
                        }
                    }
                    else
                    {
                        list = _command.ListFilesFolders();
                    }
                    foreach (String item in list)
                        result += item + "\n";
                    Console.WriteLine(result);
                }
                if (args[0] == Constant.CommandMv)
                {
                    if (args.Length == 3)
                    {
                        _command.Rename(args[1], args[2]);

                        Console.WriteLine("Archivo renombrado");
                    }
                    else
                        Console.WriteLine("Faltan parametros, formato: mv [archivo1] [archivo2]");

                }
              
                if (args[0] == Constant.CommandHelp)
                {
                    String param = args[0];
                    if (args.Length > 1)
                        param = args[1];
                    String result = string.Empty;
                    DirectoryInfo directoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory());

                    IList<Help> list = _command.Help(param);
                    foreach (Help item in list)
                        result += item.Text + "\n";
                    Console.WriteLine(result);
                }
            }
            else
            {
                String param = Constant.CommandHelp;
                String result = string.Empty;
                DirectoryInfo directoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory());

                IList<Help> list = _command.Help(param);
                foreach (Help item in list)
                    result += item.Text + "\n";
                Console.WriteLine(result);
            }

        }


    }
}


