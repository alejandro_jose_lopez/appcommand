﻿using AppCommand.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using AppCommand.entities;

namespace AppCommand
{
    public class Command : Icommand
    {
        IList<String> result = new List<String>();      

        public IList<Help> Help(string command)
        {
            IList<Help> help = new List<Help>() {
               new entities.Help() { Key="touch" ,Text="Crea un nuevo archivo, parametros: touch [nombre archivo]"},
               new entities.Help() { Key="mv" ,Text="Cambia de nombre un archivo o mueve un archivo de una carpeta a otra, parametros: mv [nombre archivo1] [nombre archivo1] o mv [path origen] [path destino]"},
               new entities.Help() { Key="ls" ,Text="Muestra los archivos/carpetas que se  encuentra en el directorio, parametros: ls , modificador -R -(Muestra el contenido de todos los subdirectorios en forma recursiva)"},
               new entities.Help() { Key="cd" ,Text="Permite navegar entre los diferentes directorios, parametro: cd"},
               };
            return help.Where(x => x.Key == command || command == "help").ToList();
        }

        public IList<string> ListFilesFolders()
        {
            IList<String> result=new List<String>();
            var files = System.IO.Directory.GetFiles(Directory.GetCurrentDirectory()).ToList();
            var directory= System.IO.Directory.GetDirectories(Directory.GetCurrentDirectory()).ToList();

            foreach (string file in files)
                result.Add(file);
            foreach (string folder in directory)
                result.Add(folder);
            return result;

        }

        public IList<string> ListRecursive(DirectoryInfo directoryInfo, int level)
        {
          
            string strPad = new String(' ', 2 * level);

            result.Add(strPad + directoryInfo.Name);

            foreach (DirectoryInfo diChild in directoryInfo.GetDirectories())
                     this.ListRecursive(diChild, level + 1);

            return result;
    }

        public void Rename(string nameFile1, string nameFile2)
        {
            FileInfo file = new FileInfo(nameFile1);
            file.MoveTo(nameFile2);
        }

        public void Touch(string nameFile)
        {
            System.IO.File.Create(nameFile);
        }

        public void ChangeDirectory(string path)
        {

            throw new NotImplementedException();
        }
    }
}
