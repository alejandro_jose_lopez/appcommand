Instructivo par configurar el comando

Repositorio:  https://gitlab.com/alejandro_jose_lopez/appcommand

1) Crear carpeta AppCommand en c:

2) Copiar el contenido de bin\Release\netcoreapp3.1 a esta carpeta

3) Agregar en el Path una entrada para C:\AppCommand
	
	Ir a Explorador de archivos, click derecho sobre "Este Equipo"
	Propiedades/Cambiar configuración/Opciones avanzadas/Variables de entorno / Path
	Doble click sobre Path / Nuevo y agregar C:\AppCommand

4) Ejecutar comando desde cualquier directorio
	
	a) AppCommand  sin parametros devuelve el help completo
	b) AppCommand help  devuelve el help completo
	c) AppCommand help [parametro] devuelve el help del parametro

	e) AppCommand touch [Nombre archivo] crea el archivo en la carpeta actual
	f) AppCommand mv  [archivo1] [archivo1]  Renombra el archivo 1 con el 2
	g) AppCommand mv [path1] [path1] mueve el archivo1 al path 2
	h) Appcommand ls  muestra los archivos y/o carpetas que se encuentran en el directorio
	i) Appcommand ls -R Muestra las carpetas y subcarpetas en forma recursiva
	j) AppCommand cd [path] permite navegar entre diferentes directorios

